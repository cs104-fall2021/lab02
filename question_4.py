# Nitel Muhtaroglu, CS104, Lab02, 2021-10-04
# GPA Calculator

cs101_credit = 6
eng101_credit = 4
math103_credit = 6
phys101_credit = 8
tll101_credit = 4

grade_a = 4.00
grade_a_minus = 3.70
grade_b_plus = 3.30
grade_b = 3.00
grade_b_minus = 2.70
grade_c_plus = 2.30
grade_c = 2.00
grade_c_minus = 1.70
grade_d_plus = 1.30
grade_d = 1.00

total_credits = cs101_credit + eng101_credit + math103_credit + phys101_credit \
    + tll101_credit
merve_gpa = (grade_a * cs101_credit + grade_b_minus * eng101_credit + grade_c_plus * math103_credit +
             grade_d_plus * phys101_credit + grade_a_minus * tll101_credit) / total_credits
ozlem_gpa = (grade_b_plus * cs101_credit + grade_c * eng101_credit + grade_a_minus * math103_credit +
             grade_d * phys101_credit + grade_a * tll101_credit) / total_credits

print("Ozlem\'s GPA is ", ozlem_gpa)
print("Merve\'s GPA is ", merve_gpa)
