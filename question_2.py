# Nitel Muhtaroglu, CS104, Lab02, 2021-10-04
# Linear motion calculator

initial_position = 12
initial_velocity = 3.5
acceleration = 9.8
time = 10

final_position = initial_position + initial_velocity *     \
    time + 1 / 2 * acceleration * time ** 2
print("The value of s is", final_position)

