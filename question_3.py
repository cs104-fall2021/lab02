# Nitel Muhtaroglu, CS104, Lab02, 2021-10-04
# This program calculates rent per person

tasdelen_rent = 1300
cekmekoy_rent = 1500
uskudar_rent = 1800
kadikoy_rent = 2000

person_count = 3
monthly_payment_percentage = 10

tasdelen_rent_per_person = tasdelen_rent * \
    (1 + (monthly_payment_percentage / 100)) / person_count
cekmekoy_rent_per_person = cekmekoy_rent * \
    (1 + (monthly_payment_percentage / 100)) / person_count
uskudar_rent_per_person = uskudar_rent * \
    (1 + (monthly_payment_percentage / 100)) / person_count
kadikoy_rent_per_person = kadikoy_rent * \
    (1 + (monthly_payment_percentage / 100)) / person_count

print("Tasdelen: ", tasdelen_rent_per_person)
print("Cekmekoy: ", cekmekoy_rent_per_person)
print("Uskudar: ", uskudar_rent_per_person)
print("Kadokoy: ", kadikoy_rent_per_person)
